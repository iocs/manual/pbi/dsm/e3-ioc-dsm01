require essioc
require adandor
require admisc
require busy
require calc

epicsEnvSet "P" "LEBT-010:PBI-Dpl-001:"
epicsEnvSet "EVR" "PBI-DPL01:Ctrl-EVR-101:"

epicsEnvSet "ANDOR_ETC_DIR" "${E3_CMD_TOP}/etc/"
epicsEnvSet "XSIZE" "1024"
epicsEnvSet "YSIZE" "255"
epicsEnvSet "NELEM" "262144"  # NELEM = XSIZE * YSIZE+1
epicsEnvSet "QSIZE" "20"

# Create an andorCCD driver
andorCCDConfig("CAM", $(ANDOR_ETC_DIR), $(serialNum=0), $(shamrockID=0))
dbLoadRecords "andorCCD.template" "P=$(P), R=CAM-, PORT=CAM, ADDR=0, TIMEOUT=1"

# SHAMROCK spectrometer
shamrockConfig("SPECT", $(shamrockID=0), $(ANDOR_ETC_DIR))
dbLoadRecords "shamrock.template" "P=$(P), R=SPECT-, PORT=SPECT, TIMEOUT=1, PIXELS=$(XSIZE)"

# Image waveform plugin
NDStdArraysConfigure("IMG", $(QSIZE), 0, "CAM")
dbLoadRecords "NDStdArrays.template" "P=$(P), R=IMG-, PORT=IMG, TYPE=Int32, FTVL=ULONG, NELEMENTS=$(NELEM), NDARRAY_PORT=CAM"

# Trace waveform plugin
# TODO: Review next couple lines. Why are they here?
#NDStdArraysConfigure("TRC", $(QSIZE), 0, "SPECT")
#dbLoadRecords "NDStdArrays.template" "P=$(P), R=TRC-, PORT=TRC, TYPE=Int32, FTVL=ULONG, NELEMENTS=$(XSIZE), NDARRAY_PORT=CAM"
#dbLoadRecords "NDStdArrays.template" "P=$(P), R=TRC-, PORT=TRC, TYPE=Int32, FTVL=ULONG, NELEMENTS=$(XSIZE), NDARRAY_PORT=SPECT"

# HDF file saving plugin
NDFileHDF5Configure("HDF", $(QSIZE), 0, "CAM")
dbLoadRecords "NDFileHDF5.template" "P=$(P), R=HDF-, PORT=HDF, NDARRAY_PORT=CAM"

# ROI plugin
NDROIConfigure("ROI", $(QSIZE), 0, "CAM")
dbLoadRecords "NDROI.template" "P=$(P), R=ROI-, PORT=ROI, NDARRAY_PORT=CAM"

# PROC plugin
NDProcessConfigure("PROC", $(QSIZE), 0, "ROI")
dbLoadRecords "NDProcess.template" "P=$(P), R=PROC-, PORT=PROC, NDARRAY_PORT=ROI"

# NDFits plugins
NDFitsConfigure("FITS1", $(QSIZE), 0, "PROC", 0, 1)
dbLoadRecords "NDFits.template" "P=$(P), R=FIT1-, PORT=FITS1, ADDR=0, TIMEOUT=1, XSIZE=$(XSIZE), NDARRAY_PORT=PROC"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT1-1-, PORT=FITS1, ADDR=0, TIMEOUT=1"

NDFitsConfigure("FITS2", $(QSIZE), 0, "PROC", 0, 2)
dbLoadRecords "NDFits.template" "P=$(P), R=FIT2-, PORT=FITS2, ADDR=0, TIMEOUT=1, XSIZE=$(XSIZE), NDARRAY_PORT=PROC"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT2-1-, PORT=FITS2, ADDR=0, TIMEOUT=1"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT2-2-, PORT=FITS2, ADDR=1, TIMEOUT=1"

NDFitsConfigure("FITS3", $(QSIZE), 0, "PROC", 0, 3)
dbLoadRecords "NDFits.template" "P=$(P), R=FIT3-, PORT=FITS3, ADDR=0, TIMEOUT=1, XSIZE=$(XSIZE), NDARRAY_PORT=PROC"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT3-1-, PORT=FITS3, ADDR=0, TIMEOUT=1"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT3-2-, PORT=FITS3, ADDR=1, TIMEOUT=1"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT3-3-, PORT=FITS3, ADDR=2, TIMEOUT=1"

NDFitsConfigure("FITS4", $(QSIZE), 0, "PROC", 0, 4)
dbLoadRecords "NDFits.template" "P=$(P), R=FIT4-, PORT=FITS4, ADDR=0, TIMEOUT=1, XSIZE=$(XSIZE), NDARRAY_PORT=PROC"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT4-1-, PORT=FITS4, ADDR=0, TIMEOUT=1"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT4-2-, PORT=FITS4, ADDR=1, TIMEOUT=1"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT4-3-, PORT=FITS4, ADDR=2, TIMEOUT=1"
dbLoadRecords "NDFitsN.template" "P=$(P), R=FIT4-4-, PORT=FITS4, ADDR=3, TIMEOUT=1"

dbLoadTemplate "$(E3_CMD_TOP)/template/doppler.substitutions" "P=$(P)"

# Statistics plugin
NDStatsConfigure("STAT", $(QSIZE), 0, "CAM")
NDTimeSeriesConfigure("STAT_TS", $(QSIZE), 0, "CAM", 0, 22)
dbLoadRecords "NDStats.template" "P=$(P), R=STAT-, PORT=STAT, NDARRAY_PORT=CAM, NCHANS=2048, XSIZE=$(XSIZE), YSIZE=$(YSIZE), HIST_SIZE=256"

set_pass0_restoreFile "$(E3_CMD_TOP)/autosave/timestamps.sav" "P=$(P), EVR=$(EVR)"
set_pass1_restoreFile "$(E3_CMD_TOP)/autosave/default_settings.sav" "P=$(P), YSIZE=$(YSIZE), HOME=$(HOME)"
set_pass1_restoreFile "$(E3_CMD_TOP)/autosave/enable_plugins.sav" "P=$(P)"

iocshLoad "$(essioc_DIR)/common_config.iocsh"
