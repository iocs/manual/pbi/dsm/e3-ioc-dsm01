```mermaid
graph LR
subgraph "Records processing order"
Energy --CPP--> Crossec --FLNK--> Num
NumOfPeaks --FLNK--> DisableFitPlugins --FLNK--> EnableFitPlugin
NumOfPeaks --CPP--> InpA
PeakNum --FLNK--> InpA --FLNK--> InpB --FLNK--> Num --FLNK--> NumSum --CPP--> Frac
InpA & InpB --CA--> Num
PeakAmplitudeActual & PeakSigmaActual --CPP--> Num
end
```
