# Doppler Shift Monitor IOC

```mermaid
graph TB
subgraph "Image processing pipeline (asyn port names)"
SPECT --> TRC
CAM --> IMG & HDF & ROI & STAT
ROI --> PROC --> FIT1 & FIT2 & FIT3 & FIT4
end
```

## Camera & spectrograph issues: shared memory segment

It may happen that AndorSDK entry under /dev/shm gets corrupted after unclean IOC shutdown.  
In such case any subsequent IOC attempts to talk to the camera fail with AndorSDK error as follows:

	2018/08/27 12:01:20.153 andorCCD:AndorCCD: ERROR: Unknown error code=20992 returned from Andor SDK.

code 20992 meaning `DRV_NOT_AVAILABLE`. This error persists until the OS is restarted.

### Solution

If the IOC exits via the `exit` command then the SHM entry is remove from the system.  
Now, if the IOC is stopped via a signal (`Ctrl+c` or `Ctrl+d`) then the SHM is not cleaned but IOC is (always?) still able to recover communication with the camera.  
However if `kill -9 <PID>` is used, then we start getting the Andor SDK error(s).

The error response comes quickly, without any USB communication involved. This hints to a pure software (SDK) issue, not a camera related/specific issue.  
By looking at the *libandor.so* library one can notice the usage of linux SHM concepts. It is assumed due to the fact that many cameras might be attached and each is controlled by the library inside its 'camera context'.

After some googling it turns out we can use ipcXXX utilities to inspect shared memory status.  
This SHM info is seen after IOC has started (successfully or not):

	[dev@dsm-ipc ~]$ ipcs

	------ Message Queues --------
	key        msqid      owner      perms      used-bytes   messages

	------ Shared Memory Segments --------
	key        shmid      owner      perms      bytes      nattch     status
	0xffffffff 98304      dev        666        488        0

	------ Semaphore Arrays --------
	key        semid      owner      perms      nsems


The solution is to remove the shared memory entry before the IOC starts.  
We can remove the SHM segment with the following:

	[dev@dsm-ipc ~]$ ipcrm --shmem-key 0xffffffff

And if we list the shared memory segments again, it should be empty.  
Now the the IOC should be able to talk to the camera without rebooting the OS or power cycling the camera.

	[dev@dsm-ipc ~]$ ipcs
	------ Message Queues --------
	key        msqid      owner      perms      used-bytes   messages

	------ Shared Memory Segments --------
	key        shmid      owner      perms      bytes      nattch     status

	------ Semaphore Arrays --------
	key        semid      owner      perms      nsems

This procedure is invoked in [st.cmd](st.cmd) using the 'system' command.

## Camera & spectrograph issues: stuck grating

During development it was observed (many times) that the control of the grating in the spectrograph was not possible.  
The built-in microcontroller that talks over USB is not performing the movement of the grating even if instructed so via software request.

### Solution

The current best solution is to power cycle the shamrock spectrograph unit and then restart the IOC to regain control over the grating movement.

## IOC template

This project was generated with the [E3 cookiecutter IOC template](https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-ioc).
