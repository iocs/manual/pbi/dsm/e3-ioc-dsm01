# Note on andorSDK files

This IOC includes only the files necessary for the Andor Newton CCD model DU 9 20P camera.  
If you need the full andorSDK package, it can be found [here](https://jira.esss.lu.se/browse/INFRA-5893).
